set -x PATH $PATH /opt/software/swift-dev-tools/swift/bin
set -x LD_LIBRARY_PATH $LD_LIBRARY_PATH $HOME/lib/swift
set -x MANPATH $MANPATH /opt/software/swift-dev-tools/swift/man
